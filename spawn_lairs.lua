--- Nodes

minetest.register_node("draconis:spawn_fire_nest", {
	description = "spawn_fire_nest",
	tiles = {"draconis_temp_heat.png",},
	is_ground_content = true,
	drop = '',
})

minetest.register_node("draconis:spawn_ice_nest", {
	description = "spawn_ice_nest",
	tiles = {"draconis_temp_cold.png",},
	is_ground_content = true,
	drop = '',
})

--- Register Ores ---

 minetest.register_ore({
     ore_type = "scatter",
     ore = "draconis:spawn_fire_nest",
     wherein = "default:dirt_with_grass",
     clust_scarcity = 200*200*200,
     clust_num_ores = 1,
     clust_size = 1,
     height_min = 60,
     height_max = 500,
})

 minetest.register_ore({
     ore_type = "scatter",
     ore = "draconis:spawn_fire_nest",
     wherein = "default:dirt_with_dry_grass",
     clust_scarcity = 200*200*200,
     clust_num_ores = 1,
     clust_size = 1,
     height_min = 60,
     height_max = 500,
})

 minetest.register_ore({
     ore_type = "scatter",
     ore = "draconis:spawn_ice_nest",
     wherein = "default:dirt_with_snow",
     clust_scarcity = 180*180*180,
     clust_num_ores = 1,
     clust_size = 1,
     height_min = 80,
     height_max = 500,
})

 minetest.register_ore({
     ore_type = "scatter",
     ore = "draconis:spawn_ice_nest",
     wherein = "default:dirt_with_coniferous_litter",
     clust_scarcity = 300*300*300,
     clust_num_ores = 1,
     clust_size = 1,
     height_min = 30,
     height_max = 500,
})

--- Schematics ---

local my_schematics = {
	["draconis:spawn_ice_nest"] = minetest.get_modpath("draconis").."/schems/draconis_ice_nest.mts",
	["draconis:spawn_fire_nest"] = minetest.get_modpath("draconis").."/schems/draconis_fire_nest.mts",
}

minetest.register_lbm({
	name = "draconis:spawn_dragon_nest_lbm",
	nodenames = {"draconis:spawn_ice_nest", "draconis:spawn_fire_nest"},
	run_at_every_load = true,
	action = function(pos, node, active_object_count, active_object_count_wider)
		minetest.place_schematic(pos, my_schematics[node.name], 0, {}, true)
	        minetest.set_node(pos, {name = "air"})
	end,
})

